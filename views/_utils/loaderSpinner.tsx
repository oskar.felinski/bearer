import {Component} from '@bearer/core'

@Component({
    tag: 'loader-spinner',
    styleUrl: 'loaderSpinner.css',
    shadow: true
})


export class LoaderSpinner {
    render() {
        return <div class="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    }
}