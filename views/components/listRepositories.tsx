import {BearerFetch, Component, Intent, Prop} from '@bearer/core'

@Component({
  tag: 'list-repositories',
  shadow: true
})


export class ListRepositories {
  @Intent('listRepositories') fetcher: BearerFetch
  @Prop() next: (data: any) => void

  render() {
    return <list-component fetcher={this.fetcher} next={this.next} renderAttr="full_name"/>
  }
}