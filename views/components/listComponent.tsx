import {BearerFetch, Component, State, Prop} from '@bearer/core'

interface Repo {
    full_name: string
}

@Component({
    tag: 'list-component',
    styleUrl: 'listRepositories.css',
    shadow: true
})


export class ListComponent {
    @Prop() fetcher: BearerFetch
    @Prop() next: (data: any) => void
    @Prop() renderAttr: string
    @State() list: Repo[]

    async componentDidLoad() {
        this.list = await this.fetchAction();
    }

    async fetchAction() {
        const data = await this.fetcher()
        return data.data
    }

    render() {
        if (!this.list) return <loader-spinner/>
        else if (!this.list.length) return <div>No data available </div>
        else return (
                <div class="repo-container">
                    {this.list.map(value => {
                        return <div onClick={() => this.next(value)} class="repo">{value[this.renderAttr]}</div>
                    })}
                </div>
            )
    }
}