import {BearerFetch, Component, Intent, Prop} from '@bearer/core'

@Component({
    tag: 'list-pull-requests',
    shadow: true
})
export class ListPullRequests {
    @Intent('listPullRequests') fetcher: BearerFetch
    @Prop() repository: any
    @Prop() next: (data: any) => void


    getPullRequest = (params = {}): Promise<any> => {
        return this.fetcher({...params, fullName: this.repository.full_name})
    }

    render() {
        return <list-component fetcher={this.getPullRequest} next={this.next} renderAttr="title"/>
    }
}