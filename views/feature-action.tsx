import { RootComponent, Output, Prop } from '@bearer/core'
import '@bearer/ui'

// import the PullRequest type from the types.ts
import { PullRequest } from './types'

@RootComponent({
    role: 'action',
    group: 'feature'
})
export class FeatureAction {
    // Output is named pullRequests and is an Array of PullRequest
    @Output() pullRequests: PullRequest[] = []
    @Prop() next: (data: any) => void


    // The action we are going to trigger when the scenario will be completed (last screen reached)
    attachPullRequest = ({ data, complete }): void => {
        this.pullRequests = [...this.pullRequests, data.pullRequest]
        complete()
    }

    render() {
        return (
            <bearer-navigator
                btnProps={{ content: 'Feature Action', kind: 'primary' }}
                direction="right"
                complete={this.attachPullRequest}
            >
                <bearer-navigator-screen navigationTitle="Repositories" name="repository" renderFunc={({next}) => <list-repositories next={next}/>}/>
                <bearer-navigator-screen
                    // data will be passed to list-pull-requests as 'this' keyword
                    renderFunc={({ data, next }) => <list-pull-requests {...data} next={next} />}
                    name="pullRequest"
                    navigationTitle={data => data.repository.full_name}
                />
            </bearer-navigator>
        )
    }
}