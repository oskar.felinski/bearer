// We import Input
import { RootComponent, Input } from '@bearer/core'
import '@bearer/ui'

// We import the PullRequest type from types.ts
import { PullRequest } from './types'

@RootComponent({
  role: 'display',
  group: 'feature'
})
export class FeatureDisplay {
  // Input is named pullRequests and is an Array of PullRequest
  @Input() pullRequests: PullRequest[] = []

  render() {
    if (!this.pullRequests.length) {
      return <bearer-alert kind="info">No Pull Requests attached</bearer-alert>
    }

    return (
        <ul>
          {this.pullRequests.map(pr => (
              <li class="list-item">{pr.title}</li>
          ))}
        </ul>
    )
  }
}