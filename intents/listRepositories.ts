import { FetchData, TOAUTH2AuthContext, TFetchDataCallback } from '@bearer/intents'
import Client from './client'

export default class ListRepositoriesIntent {
  static intentType: any = FetchData

  
  static action(context: TOAUTH2AuthContext, params: any, body: any, callback: TFetchDataCallback) {
    //... your code goes here
    // use the client defined in client.ts to fetch real object like that:
    Client(context.authAccess.accessToken).get('/user/repos').then(({ data }) => {
        callback({ data })
      })
      .catch((error) => {
        callback({ error: error.toString() })
      })
  }
}
